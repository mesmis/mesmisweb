<div id="leftSideBar">
  <ul>
    <li style="border: none">
      <div><a class="arrow" href="#1">>></a><a href="#1">MESMIS Project</a></div>
      <ul>
        <li style="border-top: 1px dashed #666"><div><a class="arrow" href="#1.1">&gt;&gt;</a><a href="#1.1">What is the MESMIS project? </a></div></li>
        <li><div><a class="arrow" href="#1.2">>></a><a href="#1.2">Project results</a></div></li>
        <li><div><a class="arrow" href="#1.3">>></a><a href="#1.3">Participating Institutions</a></div></li>
        <li><div><a class="arrow" href="#1.4">>></a><a href="#1.4">Working team </a></div></li>
      </ul>
    </li>
  </ul>
</div>
<div>
  <div id="staticContent">
    <a name="1"></a>
    <h1><strong>MESMIS</strong> PROJECT</h1>
    <div class="separator"></div>
    <div class="section">
      <a name="1.1"></a>
      <h2>WHAT IS THE MESMIS PROJECT?</h2>
      <p> The MESMIS Project is carried out by a group of professionals and researchers from different areas who are interested in the development and dissemination of tools for the evaluation of sustainability of natural resources management systems. The project objectives focus on the development and update of the Framework for the Evaluation of Natural Resource Management Systems Incorporating Sustainability Indicators (MESMIS) and on its application in case studies throughout the rural sector, mainly in small farmer contexts of Latin America. Furthermore, the Project aims to train individuals and organizations interested in evaluating sustainability and in preparing human resources through graduate and undergraduate university programs.</p>
      <p> The project was designed to cover four main fields: research, documentation, training and application, that cover four broad and closely-integrated areas of action:</p>
      <h3>1. Research in emergent topics of sustainability</h3>
      <p> where a critical analysis of the opportunities and challenges for rendering the concept of sustainability operative is carried out. The project has delved into the following research areas:</p>
      <ul>
        <li>Analysis of sustainability evaluation frameworks</li>
        <li>Characterization of management systems</li>
        <li>Sustainability Indicators</li>
        <li>Integration of Sustainability Indicators</li>
        <li>Multi-scale Analysis</li>
        <li>Dynamic Complex Systems</li>
  <li>Companion Modeling </li>
        <li>Decision-making</li>
      </ul>
      <h3>2. Application and synthesis of case studies</h3>
      <p> Systemizing experiences in the application of the MESMIS framework has been one of the principal concerns of the project. The case studies document how the methodology has been applied in diverse systems, ecological contexts and socioeconomic situations, from the southern tip of South America to the Iberian Peninsula. Among these experiences, there are case studies of small-scale wine production systems in Argentina and Portugal, diversified multi-level ecological production in the Andes and communal management of temperate forests and rainforests, silvo-pasture agroforestry systems and small-scale ranching in Mexico.</p>
      <h3>3. Training</h3>
      <p> This area has been a pillar of the MESMIS project and has helped to train a wide group of persons emphasizing the multi-dimensionality of natural resources management and its associated ecosystems. The training program has tried to cover diverse educational spheres: university undergraduate and graduate programs in Mexico and Spain, international courses and training workshops for civil servants, technicians and rural promoters.</p>
      <h3>4. Generation of reference materials</h3>
      <p> The MESMIS project also creates reference materials to provide theoretical and methodological basis for understanding the concept and evaluating sustainability in order to promote its discussion and application. These materials include publications in the form of refereed articles, books, reports and working documents. </p>
      <p>For detailed information about the MESMIS project, research areas and systematized case studies, see Astier et al. 2010 <a
href="http://www.ciga.unam.mx/ciga/images/stories/publicaciones/sustentabilidad/GIRA_CS3_final.pdf"
target="blank">Astier et al. 2010.</a></p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
    <div class="section">
      <a name="1.2"></a>
      <h2>PROJECT IMPACT</h2>
      <p>Since its first edition in 1999, the MESMIS project has had a significant impact:</p>
      <ul>
        <li>50 case studies in Mexico, Latin America and Spain (shown on map)</li>
        <li>More than 30 publications, including refereed articles and books, handbooks, non specialized publications, and educational support material.</li>
        <li>40 courses, workshops and seminars taught in Latin America.</li>
        <li>Participation in 15 undergraduate and graduate programs in Latin America and Spain.</li>
        <li>Collaboration in international teams, including the FAO FESLM project.</li>
        <li>Development of MESMIS Interactive, didactic, user-friendly and interactive software, about how to apply the MESMIS framework.</li>
      </ul>
      <?php echo image_tag('static/mesmis-casestudies.png', 'alt="DistribuciÃ³n de los estudios de caso de
MESMIS"') ?>
      <p> MESMIS case studies. For more information about the framework case studies, please refer to the Sustainability Evaluation Collection<a
href="http://mesmis.gira.org.mx/es/products#pg122" target="blank">Sustainability Evaluation Collection</a></p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
    <div class="section">
      <a name="1.3"></a>
      <h2>INSTITUTIONAL PARTNERSHIP</h2>
      <p> A central objective of the MESMIS project is the consolidation of a multi-institutional and interdisciplinary research team, where institutions and organizations involved in natural resources management come together. This group consists of four academic institutions and one NGO: Centro de Investigaciones en Ecosistemas (CIECO-UNAM), Centro de Investigaciones en Geografía Ambiental (CIGA-UNAM), Centro de Investigaciones en Ciencias Agropecuarias (CICA-UAEM), El Colegio de la Frontera Sur (ECOSUR) and the Grupo Interdisciplinario de Tecnología Rural Apropiada (GIRA, A.C.).</p>
      <p> This group of institutions collaborates in undergraduate and graduate courses in different universities, workshops, and in the generation of national and international publications. Also, all members advise undergraduate and graduate theses and dissertations as well as local, national and international studies.</p>
      <p>Simultaneously, each member keeps close ties with Universities and Research institutions in Latin America, Spain, Holland and France.</p>
      <p>The MESMIS research team has helped to strengthen the theoretical foundations of sustainability evaluation and its application in concrete experiences. Today, the team is in a stage of consolidation, and its goal is to improve the conceptual and practical tools for sustainability evaluation. </p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
    <div class="section">
      <a name="1.4"></a>
      <h2>THE TEAM</h2>
      <p align="center"><?php echo image_tag('static/about_us_team.jpg', 'alt="Equipo de trabajo"') ?></p>
      <h3>Dr. Marta Astier</h3>
      <p> Marta is a researcher at the Centro de Investigaciones en Geografía Ambiental (CIGA-UNAM). She has published several peer-reviewed publications on ecological agriculture, particularly, soil quality and sustainability of small farmer’s production systems. She focuses on ecological indicators, “trade-offs”, and systemization of case studies that use the MESMIS framework. </p>
      <h3>Dr. Omar Masera</h3>
      <p> Omar is a researcher at the Centro de Investigaciones en Ecosistemas (CIECO-UNAM) and advisor of GIRA A.C. His field of work is in energy and natural resources, global climate change, and sustainability. He focuses on developing the theoretical and conceptual framework of MESMIS, emphasizing on the multidimensional aspects of the framework. </p>
      <h3>Dr. Luis Garc&iacute;a Barrios</h3>
      <p> Luis is a researcher at El Colegio de la Frontera Sur (ECOSUR), San Cristóbal de las Casas campus (Chiapas). His area of expertise in the project is the theoretical foundations of complex systems. He also designs: interactive simulation models as tutorials to teach complex and dynamic systems and companion modeling for stakeholders in conflict. </p>
      <h3>Dr. Carlos Gonz&aacute;lez Esquivel</h3>
      <p> Carlos is a researcher in School of Biological Sciences, University of East Anglia, Norwich, UK. He has developed research focused on the sustainability of livestock production systems. His work has been centered in the generation of theoretical and practical tools in the sphere of livestock indicators and their application in diverse case studies of the Valle de Toluca, Mexico.</p>
      <h3>Dr. Santiago L&oacute;pez-Ridaura</h3>
      <p> Santiago is a researcher in the Institut National de la Recherche Agronomique in Montpellier, France. He also collaborates as an advisor for GIRA A.C. His work has focused on the evaluation and design of innovative agricultural systems integrating crop association, as well as on the development and implementation of multi-scale analysis at the regional scale.</p>
      <h3>MSc. Tamara Ortiz</h3>
      <p> Tamara works in the Outreach unit of the CIECO-UNAM, where she promotes articulation of research with society’s concrete problems. She has participated in the MESMIS project for several years and has been a key member in the promotion and organization of courses and workshops on the framework. Her interest topics are the characterization of the management systems and studies of the participatory processes in sustainability evaluations.</p>
      <h3>MSc. Yankuic Galv&aacute;n-Miyoshi</h3>
      <p> Yankuic currently works for the Universidad Aut&oacute;noma de Chapingo at Texcoco, Estado de Mexico, where he is participating in regional and local sustainability assessments for biofuel feedstock production in Mexico. His area of specialization in the project is on multi-criteria methods for the integration of the sustainability indicators.</p>
      <div class="backToTop"><a href="#" onclick="gotoTop();return false"><?php echo __('Regresar') ?></a></div>
    </div>
  </div>
</div>