<div id="main">
  <h1>Credits</h1>
  <div class="separator"></div>
  <p>
     Content and general design<br />
     <strong>Yankuic Galván-Miyoshi</strong>
  </p>
  <p>
     Graphic Design<br />
     <strong>Raul Montero</strong>
  </p>
  <p>
     Photography<br />
     <strong>Yankuic Galván-Miyoshi</strong><br />
     <strong>Andrés Camou Guerrero </strong>
  </p>
  <p>
     Software and website development<br />
     <strong>Max Pimm</strong><br />
     <strong>Simó Albert</strong>
  </p>
  <p>
     Translations from Spanish<br />
     <strong>Gabriela Tobias</strong><br />
     <strong>Diane Miyoshi</strong>
  </p>
  <div class="separator"></div>
  <p>
     The development and maintenance of this site has been possible with the financial suppport of the CONACYT project 51293 <em>Estudios de Sustentablidad de Sistemas Complejos Socioambientales</em>.
  </p>
</div>
